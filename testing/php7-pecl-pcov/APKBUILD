# Contributor: Andy Postnikov <apostnikov@gmail.com>
# Maintainer: Andy Postnikov <apostnikov@gmail.com>

pkgname=php7-pecl-pcov
_extname=pcov
pkgver=1.0.7
pkgrel=0
pkgdesc="Code coverage driver for PHP 7 - PECL"
url="https://pecl.php.net/package/pcov"
arch="all"
license="PHP-3.01"
depends="php7-common"
makedepends="php7-dev"
source="php-pecl-$_extname-$pkgver.tgz::https://pecl.php.net/get/$_extname-$pkgver.tgz"
builddir="$srcdir"/$_extname-$pkgver

build() {
	phpize7
	./configure --prefix=/usr --with-php-config=php-config7
	make
}

check() {
	php7 -dextension=modules/$_extname.so --ri $_extname
	make NO_INTERACTION=1 REPORT_EXIT_STATUS=1 test TESTS=--show-diff
}

package() {
	make INSTALL_ROOT="$pkgdir" install

	local _confdir="$pkgdir"/etc/php7/conf.d
	mkdir -p $_confdir
	echo "extension=$_extname" > $_confdir/$_extname.ini
}

sha512sums="fb09ffe08dc8d0491a5113f220b0aefa132601a0d984ae0f972fb10cedefad4ec6d66830c773077c088ac051692970ad46958b551f335ab9697d1de0d2a04d4e  php-pecl-pcov-1.0.7.tgz"
